

/* Program to convert big endian to little endian and vice versa
    Author: Amit Malyala (C) 2006 Amit Malyala. */
/* include custom data types */
#include "std_types.h"
#include "cstdio"

/* Change endianness */
SINT32 ChangeEndianness(SINT32 *number);

/* Function main */
SINT32 main(void)
{
    SINT32 number= 0x12345678;
    printf("\n%x",number);
    printf("\nLittle endian number: ");
    printf("\n%x\n",ChangeEndianness(&number));
    
    return 0;
}

SINT32 ChangeEndianness(SINT32 *value)
{
    SINT32 temp=0;
    temp |= (*value & (0x000000FF)) << 24;
    temp |= (*value & (0x0000FF00)) << 8;
    temp |= (*value & (0x00FF0000)) >> 8;
    temp |= (*value & (0xFF000000)) >> 24;
    return temp;
}